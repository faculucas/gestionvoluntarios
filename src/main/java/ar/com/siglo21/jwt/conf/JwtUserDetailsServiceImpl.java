package ar.com.siglo21.jwt.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ar.com.siglo21.model.Usuario;
import ar.com.siglo21.repositories.UsuarioRepository;

/**
 * 
 * @author sj
 *
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository userRepo;
	
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario u = userRepo.findByUsernameAndActivoIsTrue(username).orElse(null);
		if(u== null){
			throw new UsernameNotFoundException(username);
		}
        return u;
    }
}
