package ar.com.siglo21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.DiasDisponibles;

public interface DiasDisponiblesRepository extends JpaRepository<DiasDisponibles, Long> {

}
