package ar.com.siglo21.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	Optional<Usuario> findByUsernameAndActivoIsTrue(String username);

}
