package ar.com.siglo21.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.Voluntario;

public interface VoluntarioRepository extends JpaRepository<Voluntario, Long> {
	
	List<Voluntario> findByNombreContainingOrderByNombre(String nombre);
	
	Voluntario findByDni(String dni);
	
	long countByFechaModificacionConvenioIsNullOrFechaModificacionAnexoIsNull();
	
	List<Voluntario> findByFechaModificacionConvenioIsNullOrFechaModificacionAnexoIsNullOrderByNombre();

}
