package ar.com.siglo21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.LugarTrabajo;

public interface LugarTrabajoRepository extends JpaRepository<LugarTrabajo, Long> {

}
