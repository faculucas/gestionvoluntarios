package ar.com.siglo21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.EstadoInscripcion;

public interface EstadoInscripcionRepository extends JpaRepository<EstadoInscripcion, Long> {

}
