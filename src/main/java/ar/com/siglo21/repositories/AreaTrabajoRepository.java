package ar.com.siglo21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.AreaTrabajo;

public interface AreaTrabajoRepository extends JpaRepository<AreaTrabajo, Long> {

}
