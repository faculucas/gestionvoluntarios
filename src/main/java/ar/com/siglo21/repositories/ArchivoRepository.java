package ar.com.siglo21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.Archivo;

public interface ArchivoRepository extends JpaRepository<Archivo, String> {

}
