package ar.com.siglo21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.siglo21.model.Parametros;

public interface ParametrosRepository extends JpaRepository<Parametros, Long> {

	Parametros findByCodigo(String codigo);
	
}
