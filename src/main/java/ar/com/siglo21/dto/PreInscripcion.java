package ar.com.siglo21.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreInscripcion {

	private String nombre;
	
	private String email;
	
	private String celular;
	
}
