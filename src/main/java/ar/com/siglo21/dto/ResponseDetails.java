package ar.com.siglo21.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseDetails {

	private Date timestamp;
    
	private String message;
    
}
