package ar.com.siglo21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionVoluntariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionVoluntariosApplication.class, args);
	}

}
