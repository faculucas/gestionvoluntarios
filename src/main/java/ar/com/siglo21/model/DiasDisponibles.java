package ar.com.siglo21.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table
public class DiasDisponibles {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean lunes;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean martes;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean miercoles;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean jueves;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean viernes;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean sabado;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean domingo;
	
	@Column(columnDefinition = "boolean default false", nullable=false)
	private Boolean feriados;

}
