package ar.com.siglo21.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table
public class Voluntario{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nombre;
	
	private String dni;
	
	private Date fechaNacimiento;
	
	private String sexo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="area_trabajo_id")
	private AreaTrabajo areaTrabajo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="lugar_trabajo_id")
	private LugarTrabajo lugarTrabajo;
	
	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL,optional=false)
	private DiasDisponibles diasDisponibles;
	
	private String domicilio;
	
	private String email;
	
	private String celular;
	
	private String experiencia;
	
	private Integer desde;
	
	private Integer hasta;
	
	private String estadoCivil;
	
	private String nacionalidad;
	
	private String situacionOcupacional;
	
	private String estudios;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JsonIgnore
	private Archivo convenio;
	
	private Date fechaModificacionConvenio;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JsonIgnore
	private Archivo anexo;
	
	private Date fechaModificacionAnexo;
	
	@Transient
	private Boolean vencido;
	
	@PostLoad
	private void onLoad() {
		
		if(fechaModificacionConvenio!=null) {
			
			Calendar c = Calendar.getInstance();
	        c.setTime(fechaModificacionConvenio);
	        c.add(Calendar.YEAR, 1);
	        Date datePlusOne = c.getTime();
	        
	        Date today = new Date();
	        
	        this.vencido = today.after(datePlusOne);
	        
		} else {
			this.vencido = false;
		}
	}
	
}
