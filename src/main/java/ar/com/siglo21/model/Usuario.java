package ar.com.siglo21.model;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ar.com.siglo21.model.enums.RolEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table
public class Usuario implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "authentication_id", unique = true)
	private String uid;
	private String username;
	private String email;
	private String password;
	private String nombre;
	private String apellido;
	private Boolean activo;
	private Boolean verificado = false;
	private Boolean bloqueado = false;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Rol> roles = new HashSet<>();

	private Date ultimaActualizacionPassword;

	private String tokenPassword;

	private String pushToken;

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return !this.bloqueado;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public boolean isEnabled() {
		return activo;
	}

	public void actualizarPassword(String nuevaPass) {
		this.password = nuevaPass;
		this.ultimaActualizacionPassword = new Date();
	}

	@JsonIgnore
	public Boolean esAdministrador() {
		return this.tieneRol(RolEnum.ADMIN);
	}

	@JsonIgnore
	public Boolean tieneRol(RolEnum r) {
		return this.roles.stream().anyMatch(p -> r.getCodigo().equals(p.getCodigo()));
	}

	@JsonIgnore
	public String getFullName() {
		return this.getNompreCompleto();
	}

	public void addRol(Rol rol) {
		this.roles.add(rol);
	}

	public void removeRol(Rol r) {
		this.roles.remove(r);
	}

	public String getNompreCompleto() {

		return String.join(" ", this.getNombre(), this.getApellido());
	}

}
