package ar.com.siglo21.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum RolEnum {
	ADMIN("ROLE_ADMIN", "Superusuario", "S"), 
	JEFE("ROLE_JEFE", "Jefe", "J"), 
	EMPLEADO("ROLE_EMPLEADO", "Empleado","E");

	private String codigo;
	private String descripcion;
	private String inicial;

	private RolEnum(String codigo, String descripcion, String inicial) {
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.inicial = inicial;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public static RolEnum byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(RolEnum.values()).filter(tc -> tc.codigo.equals(codigo)).findFirst().orElse(null);
	}

	public static RolEnum getByInicial(String r) {
		if (StringUtils.isEmpty(r)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(RolEnum.values()).filter(tc -> tc.inicial.equals(r)).findFirst().orElse(null);
	}

	public String getInicial() {
		return inicial;
	}

}
