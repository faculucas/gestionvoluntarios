package ar.com.siglo21.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ar.com.siglo21.exception.ResourceNotFoundException;
import ar.com.siglo21.model.AreaTrabajo;
import ar.com.siglo21.repositories.AreaTrabajoRepository;


@RestController
public class AreaTrabajoController {
	
	@Autowired
    private AreaTrabajoRepository areaTrabajoRepository;

    @GetMapping("/areaTrabajo")
    public List<AreaTrabajo> getAllAreaTrabajos() {
        return areaTrabajoRepository.findAll();
    }

    @GetMapping("/areaTrabajo/{id}")
    public ResponseEntity<AreaTrabajo> getAreaTrabajoById(@PathVariable(value = "id") Long areaTrabajoId)
        throws ResourceNotFoundException {
        
    	AreaTrabajo areaTrabajo = areaTrabajoRepository.findById(areaTrabajoId)
    			.orElseThrow(() -> new ResourceNotFoundException("AreaTrabajo not found for this id :: " + areaTrabajoId));
        
    	return ResponseEntity.ok().body(areaTrabajo);
    	
    }
    
    @PostMapping("/areaTrabajo")
    public AreaTrabajo createAreaTrabajo(@Valid @RequestBody AreaTrabajo areaTrabajo) {
        return areaTrabajoRepository.save(areaTrabajo);
    }

    @PutMapping("/areaTrabajo/{id}")
    public ResponseEntity<AreaTrabajo> updateAreaTrabajo(@PathVariable(value = "id") Long areaTrabajoId,
        @Valid @RequestBody AreaTrabajo areaTrabajoDetails) throws ResourceNotFoundException {
        
    	AreaTrabajo areaTrabajo = areaTrabajoRepository.findById(areaTrabajoId)
    			.orElseThrow(() -> new ResourceNotFoundException("AreaTrabajo not found for this id :: " + areaTrabajoId));

        areaTrabajo.setCodigo(areaTrabajoDetails.getCodigo());
        areaTrabajo.setDescripcion(areaTrabajoDetails.getDescripcion());
        
        final AreaTrabajo updatedAreaTrabajo = areaTrabajoRepository.save(areaTrabajo);
        
        return ResponseEntity.ok(updatedAreaTrabajo);
        
    }

    @DeleteMapping("/areaTrabajo/{id}")
    public Map<String, Boolean> deleteAreaTrabajo(@PathVariable(value = "id") Long areaTrabajoId)
         throws ResourceNotFoundException {
        
    	AreaTrabajo areaTrabajo = areaTrabajoRepository.findById(areaTrabajoId)
    			.orElseThrow(() -> new ResourceNotFoundException("AreaTrabajo not found for this id :: " + areaTrabajoId));

        areaTrabajoRepository.delete(areaTrabajo);
       
        Map<String, Boolean> response = new HashMap<>();
        
        response.put("deleted", Boolean.TRUE);
        
        return response;
        
    }
	
}
