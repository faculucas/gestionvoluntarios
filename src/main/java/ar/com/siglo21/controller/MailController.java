package ar.com.siglo21.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.siglo21.dto.PreInscripcion;
import ar.com.siglo21.dto.ResponseDetails;
import ar.com.siglo21.exception.ResourceNotFoundException;
import ar.com.siglo21.model.Archivo;
import ar.com.siglo21.model.Parametros;
import ar.com.siglo21.model.Voluntario;
import ar.com.siglo21.repositories.ParametrosRepository;
import ar.com.siglo21.repositories.VoluntarioRepository;
import ar.com.siglo21.service.ArchivoService;
import ar.com.siglo21.service.EmailService;

@RestController
@RequestMapping("mail")
public class MailController {
	
	@Autowired
    private ArchivoService archivoService;
	
	@Autowired
    private ParametrosRepository parametrosRepository;

	@Autowired
	private EmailService emailService;
	
	@Autowired
    private VoluntarioRepository voluntarioRepository;
	
	@GetMapping("/send")
    public ResponseEntity<String> sendMail() {
        emailService.sendSimpleMessage("flucas@conexia.com", "Test", "Prueba");
		return ResponseEntity.ok("Se realizo el envio correctamente!");
    }
	
	@GetMapping("/send/convenio/{id}")
    public ResponseEntity<ResponseDetails> sendConvenioToVoluntario(@PathVariable(value = "id") Long voluntarioId) throws ResourceNotFoundException {
		
		Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));
    	
		Parametros convenio = parametrosRepository.findByCodigo("CONVENIO");
    	
        // Load file from database
    	Archivo dbFile = archivoService.getFile(convenio.getValor());
		
        emailService.sendMessageWithAttachment(voluntario.getEmail(), "Convenio", "Por favor imprima el convenio, firmelo y envielo nuevamente, puede ser una foto o un scan. Muchas gracias!", dbFile);
        
        return ResponseEntity.ok(new ResponseDetails(new Date(),"Se realizo el envio correctamente!"));
    }
	
	@PostMapping("/send/preinscripcion")
    public ResponseEntity<ResponseDetails> createVoluntario(@Valid @RequestBody PreInscripcion pri) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		String dateString = format.format(new Date());
		
		String html = 	"<table>\n" + 
						"  <tr>\n" + 
						"    <td>Fecha:</td>\n" + 
						"    <td><strong>" + dateString + "</strong></td>\n" + 
						"  </tr>\n" + 
						"  <tr>\n" + 
						"    <td>Nombre:</td>\n" + 
						"    <td><strong>" + pri.getNombre() + "</strong></td>\n" + 
						"  </tr>\n" + 
						"  <tr>\n" + 
						"    <td>Celular:</td>\n" + 
						"    <td><strong>" + pri.getCelular() + "</strong></td>\n" + 
						"  </tr>\n" + 
						"  <tr>\n" + 
						"    <td>Email:</td>\n" + 
						"    <td><strong>" + pri.getEmail() + "</strong></td>\n" + 
						"  </tr>\n" + 
						"</table>";
		
		emailService.sendMessageHTML("flucas@conexia.com", "Formulario de Pre-Inscripcion", html);
		return ResponseEntity.ok(new ResponseDetails(new Date(),"Se realizo el envío correctamente, pronto nos pondremos en contacto con vos. Muchas gracias!"));
    }
	
}
