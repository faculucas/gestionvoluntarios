package ar.com.siglo21.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import ar.com.siglo21.dto.ResponseDetails;
import ar.com.siglo21.exception.ResourceNotFoundException;
import ar.com.siglo21.model.Archivo;
import ar.com.siglo21.model.Parametros;
import ar.com.siglo21.model.Voluntario;
import ar.com.siglo21.repositories.ParametrosRepository;
import ar.com.siglo21.repositories.VoluntarioRepository;
import ar.com.siglo21.service.ArchivoService;

import java.util.Date;

@RestController
@RequestMapping("archivo")
public class ArchivoController {

	@Autowired
    private ArchivoService archivoService;
	
	@Autowired
    private ParametrosRepository parametrosRepository;
	
	@Autowired
    private VoluntarioRepository voluntarioRepository;

    @PostMapping("/upload")
    public ResponseEntity<ResponseDetails> uploadFile(@RequestParam("file") MultipartFile file) {
    	
    	archivoService.storeFile(file);

    	return ResponseEntity.ok(new ResponseDetails(new Date(),"Se realizo el envio correctamente!"));
    }

    @GetMapping("/download/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
        // Load file from database
    	Archivo dbFile = archivoService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }
    
    @GetMapping("/download/convenio")
    public ResponseEntity<Resource> downloadConvenio() {
    	
    	Parametros convenio = parametrosRepository.findByCodigo("CONVENIO");
    	
        // Load file from database
    	Archivo dbFile = archivoService.getFile(convenio.getValor());

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }
    
    @PostMapping("/upload/convenio")
    public ResponseEntity<ResponseDetails> uploadConvenio(@RequestParam("file") MultipartFile file) {
    	
    	Archivo dbFile = archivoService.storeFile(file);
    	
    	Parametros convenio = parametrosRepository.findByCodigo("CONVENIO");
    	
    	convenio.setValor(dbFile.getId());
    	
    	parametrosRepository.save(convenio);

    	return ResponseEntity.ok(new ResponseDetails(new Date(),"Se realizo el envio correctamente!"));
    }
    
    @PostMapping("/upload/convenio/{id}")
    public ResponseEntity<ResponseDetails> uploadConvenioVoluntario(@RequestParam("file") MultipartFile file, 
    		@PathVariable(value = "id") Long voluntarioId) throws ResourceNotFoundException{
    	
    	Archivo dbFile = archivoService.storeFile(file);
    	
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));

    	
    	voluntario.setConvenio(dbFile);
    	voluntario.setFechaModificacionConvenio(new Date());
    	
    	voluntarioRepository.save(voluntario);

    	return ResponseEntity.ok(new ResponseDetails(new Date(),"Se realizo el envio correctamente!"));
    }
    
    @PostMapping("/upload/anexo/{id}")
    public ResponseEntity<ResponseDetails> uploadAnexoVoluntario(@RequestParam("file") MultipartFile file, 
    		@PathVariable(value = "id") Long voluntarioId) throws ResourceNotFoundException{
    	
    	Archivo dbFile = archivoService.storeFile(file);
    	
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));

    	
    	voluntario.setAnexo(dbFile);
    	voluntario.setFechaModificacionAnexo(new Date());
    	
    	voluntarioRepository.save(voluntario);

    	return ResponseEntity.ok(new ResponseDetails(new Date(),"Se realizo el envio correctamente!"));
    }
    
    @GetMapping("/download/convenio/{id}")
    public ResponseEntity<Resource> downloadConvenioVoluntario(@PathVariable(value = "id") Long voluntarioId) throws ResourceNotFoundException {
    	
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));
    	
        // Load file from database
    	Archivo dbFile = voluntario.getConvenio();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }
    
    @GetMapping("/download/anexo/{id}")
    public ResponseEntity<Resource> downloadAnexoVoluntario(@PathVariable(value = "id") Long voluntarioId) throws ResourceNotFoundException {
    	
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));
    	
        // Load file from database
    	Archivo dbFile = voluntario.getAnexo();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }
    
}
