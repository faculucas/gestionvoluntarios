package ar.com.siglo21.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ar.com.siglo21.exception.ResourceNotFoundException;
import ar.com.siglo21.model.EstadoInscripcion;
import ar.com.siglo21.repositories.EstadoInscripcionRepository;


@RestController
public class EstadoInscripcionController {
	
	@Autowired
    private EstadoInscripcionRepository estadoInscripcionRepository;

    @GetMapping("/estadoInscripcion")
    public List<EstadoInscripcion> getAllEstadoInscripcions() {
        return estadoInscripcionRepository.findAll();
    }

    @GetMapping("/estadoInscripcion/{id}")
    public ResponseEntity<EstadoInscripcion> getEstadoInscripcionById(@PathVariable(value = "id") Long estadoInscripcionId)
        throws ResourceNotFoundException {
        
    	EstadoInscripcion estadoInscripcion = estadoInscripcionRepository.findById(estadoInscripcionId)
    			.orElseThrow(() -> new ResourceNotFoundException("EstadoInscripcion not found for this id :: " + estadoInscripcionId));
        
    	return ResponseEntity.ok().body(estadoInscripcion);
    	
    }
    
    @PostMapping("/estadoInscripcion")
    public EstadoInscripcion createEstadoInscripcion(@Valid @RequestBody EstadoInscripcion estadoInscripcion) {
        return estadoInscripcionRepository.save(estadoInscripcion);
    }

    @PutMapping("/estadoInscripcion/{id}")
    public ResponseEntity<EstadoInscripcion> updateEstadoInscripcion(@PathVariable(value = "id") Long estadoInscripcionId,
        @Valid @RequestBody EstadoInscripcion estadoInscripcionDetails) throws ResourceNotFoundException {
        
    	EstadoInscripcion estadoInscripcion = estadoInscripcionRepository.findById(estadoInscripcionId)
    			.orElseThrow(() -> new ResourceNotFoundException("EstadoInscripcion not found for this id :: " + estadoInscripcionId));

    	estadoInscripcion.setCodigo(estadoInscripcionDetails.getCodigo());
    	estadoInscripcion.setDescripcion(estadoInscripcionDetails.getDescripcion());
        
        final EstadoInscripcion updatedEstadoInscripcion = estadoInscripcionRepository.save(estadoInscripcion);
        
        return ResponseEntity.ok(updatedEstadoInscripcion);
        
    }

    @DeleteMapping("/estadoInscripcion/{id}")
    public Map<String, Boolean> deleteEstadoInscripcion(@PathVariable(value = "id") Long estadoInscripcionId)
         throws ResourceNotFoundException {
        
    	EstadoInscripcion estadoInscripcion = estadoInscripcionRepository.findById(estadoInscripcionId)
    			.orElseThrow(() -> new ResourceNotFoundException("EstadoInscripcion not found for this id :: " + estadoInscripcionId));

        estadoInscripcionRepository.delete(estadoInscripcion);
       
        Map<String, Boolean> response = new HashMap<>();
        
        response.put("deleted", Boolean.TRUE);
        
        return response;
        
    }
	
}
