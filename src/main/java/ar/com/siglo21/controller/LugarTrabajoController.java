package ar.com.siglo21.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ar.com.siglo21.exception.ResourceNotFoundException;
import ar.com.siglo21.model.LugarTrabajo;
import ar.com.siglo21.repositories.LugarTrabajoRepository;


@RestController
public class LugarTrabajoController {
	
	@Autowired
    private LugarTrabajoRepository lugarTrabajoRepository;

    @GetMapping("/lugarTrabajo")
    public List<LugarTrabajo> getAllLugarTrabajos() {
        return lugarTrabajoRepository.findAll();
    }

    @GetMapping("/lugarTrabajo/{id}")
    public ResponseEntity<LugarTrabajo> getLugarTrabajoById(@PathVariable(value = "id") Long lugarTrabajoId)
        throws ResourceNotFoundException {
        
    	LugarTrabajo lugarTrabajo = lugarTrabajoRepository.findById(lugarTrabajoId)
    			.orElseThrow(() -> new ResourceNotFoundException("LugarTrabajo not found for this id :: " + lugarTrabajoId));
        
    	return ResponseEntity.ok().body(lugarTrabajo);
    	
    }
    
    @PostMapping("/lugarTrabajo")
    public LugarTrabajo createLugarTrabajo(@Valid @RequestBody LugarTrabajo lugarTrabajo) {
        return lugarTrabajoRepository.save(lugarTrabajo);
    }

    @PutMapping("/lugarTrabajo/{id}")
    public ResponseEntity<LugarTrabajo> updateLugarTrabajo(@PathVariable(value = "id") Long lugarTrabajoId,
        @Valid @RequestBody LugarTrabajo lugarTrabajoDetails) throws ResourceNotFoundException {
        
    	LugarTrabajo lugarTrabajo = lugarTrabajoRepository.findById(lugarTrabajoId)
    			.orElseThrow(() -> new ResourceNotFoundException("LugarTrabajo not found for this id :: " + lugarTrabajoId));

        lugarTrabajo.setCodigo(lugarTrabajoDetails.getCodigo());
        lugarTrabajo.setDescripcion(lugarTrabajoDetails.getDescripcion());
        
        final LugarTrabajo updatedLugarTrabajo = lugarTrabajoRepository.save(lugarTrabajo);
        
        return ResponseEntity.ok(updatedLugarTrabajo);
        
    }

    @DeleteMapping("/lugarTrabajo/{id}")
    public Map<String, Boolean> deleteLugarTrabajo(@PathVariable(value = "id") Long lugarTrabajoId)
         throws ResourceNotFoundException {
        
    	LugarTrabajo lugarTrabajo = lugarTrabajoRepository.findById(lugarTrabajoId)
    			.orElseThrow(() -> new ResourceNotFoundException("LugarTrabajo not found for this id :: " + lugarTrabajoId));

        lugarTrabajoRepository.delete(lugarTrabajo);
       
        Map<String, Boolean> response = new HashMap<>();
        
        response.put("deleted", Boolean.TRUE);
        
        return response;
        
    }
	
}
