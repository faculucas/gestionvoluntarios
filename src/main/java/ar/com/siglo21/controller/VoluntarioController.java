package ar.com.siglo21.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.siglo21.exception.ResourceNotFoundException;
import ar.com.siglo21.model.Voluntario;
import ar.com.siglo21.repositories.VoluntarioRepository;


@RestController
@RequestMapping("voluntario")
public class VoluntarioController {
	
	@Autowired
    private VoluntarioRepository voluntarioRepository;

    @GetMapping("")
    public List<Voluntario> getAllVoluntarios() {
        return voluntarioRepository.findAll(Sort.by("nombre"));
    }
    
    @GetMapping("/incompletos/count")
    public long getVoluntariosIncompletosCount() {
        return voluntarioRepository.countByFechaModificacionConvenioIsNullOrFechaModificacionAnexoIsNull();
    }
    
    @GetMapping("/incompletos")
    public List<Voluntario> getVoluntariosIncompletos() {
        return voluntarioRepository.findByFechaModificacionConvenioIsNullOrFechaModificacionAnexoIsNullOrderByNombre();
    }
    
    @GetMapping("/busqueda/{nombre}")
    public List<Voluntario> getAllVoluntariosByName(@PathVariable(value = "nombre") String nombre) {
        return voluntarioRepository.findByNombreContainingOrderByNombre(nombre);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Voluntario> getVoluntarioById(@PathVariable(value = "id") Long voluntarioId)
        throws ResourceNotFoundException {
        
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));
        
    	return ResponseEntity.ok().body(voluntario);
    	
    }
    
    @PostMapping("")
    public ResponseEntity<Voluntario> createVoluntario(@Valid @RequestBody Voluntario voluntario)
    		throws ResourceNotFoundException {
    	
    	Voluntario v = voluntarioRepository.findByDni(voluntario.getDni());
    	
    	if(v != null)
    		throw new ResourceNotFoundException("Voluntario existente con mismo DNI");
    	else
    		return ResponseEntity.ok(voluntarioRepository.save(voluntario));
        
    }

    @PutMapping("/{id}")
    public ResponseEntity<Voluntario> updateVoluntario(@PathVariable(value = "id") Long voluntarioId,
        @Valid @RequestBody Voluntario voluntarioDetails) throws ResourceNotFoundException {
        
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));

        voluntario.setAreaTrabajo(voluntarioDetails.getAreaTrabajo());
        voluntario.setCelular(voluntarioDetails.getCelular());
        voluntario.setDesde(voluntarioDetails.getDesde());
        voluntario.setDni(voluntarioDetails.getDni());
        voluntario.setDomicilio(voluntarioDetails.getDomicilio());
        voluntario.setEmail(voluntarioDetails.getEmail());
        voluntario.setEstadoCivil(voluntarioDetails.getEstadoCivil());
        voluntario.setEstudios(voluntarioDetails.getEstudios());
        voluntario.setExperiencia(voluntarioDetails.getExperiencia());
        voluntario.setFechaNacimiento(voluntarioDetails.getFechaNacimiento());
        voluntario.setHasta(voluntarioDetails.getHasta());
        voluntario.setLugarTrabajo(voluntarioDetails.getLugarTrabajo());
        voluntario.setNacionalidad(voluntarioDetails.getNacionalidad());
        voluntario.setNombre(voluntarioDetails.getNombre());
        voluntario.setSexo(voluntarioDetails.getSexo());
        voluntario.setSituacionOcupacional(voluntarioDetails.getSituacionOcupacional());
        voluntario.setDiasDisponibles(voluntarioDetails.getDiasDisponibles());
        
        Voluntario v = voluntarioRepository.findByDni(voluntario.getDni());
        
        if(v != null && v.getId() != voluntarioId)
    		throw new ResourceNotFoundException("Voluntario existente con mismo DNI");
    	else {
    		Voluntario updatedVoluntario = voluntarioRepository.save(voluntario);
    		return ResponseEntity.ok(updatedVoluntario);
    	}
        
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteVoluntario(@PathVariable(value = "id") Long voluntarioId)
         throws ResourceNotFoundException {
        
    	Voluntario voluntario = voluntarioRepository.findById(voluntarioId)
    			.orElseThrow(() -> new ResourceNotFoundException("Voluntario not found for this id :: " + voluntarioId));

        voluntarioRepository.delete(voluntario);
       
        Map<String, Boolean> response = new HashMap<>();
        
        response.put("deleted", Boolean.TRUE);
        
        return response;
        
    }
	
}
