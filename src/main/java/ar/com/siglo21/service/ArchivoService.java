package ar.com.siglo21.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import ar.com.siglo21.exception.FileStorageException;
import ar.com.siglo21.exception.MyFileNotFoundException;
import ar.com.siglo21.model.Archivo;
import ar.com.siglo21.repositories.ArchivoRepository;

@Service
public class ArchivoService {

	@Autowired
    private ArchivoRepository archivoRepository;

    public Archivo storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            Archivo archivo = new Archivo(fileName, file.getContentType(), file.getBytes());

            return archivoRepository.save(archivo);
            
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Archivo getFile(String fileId) {
        return archivoRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }
    
}
